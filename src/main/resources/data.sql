insert into country (name, is_moderation) values ('Russia', true), ('USA', true);
insert into region (name, is_moderation, country_id) values ('Bashkortostan', true, 1), ('Komi', true, 1),
            ('Wisconsin', true, 2), ('Pennsylvania', true, 2);
insert into city (name, is_moderation, region_id) values ('Ufa', true, 1), ('Sterlitamak', true, 1),
            ('Pechora', true, 2), ('Usinsk', true, 2), ('Madison', true, 3), ('Philadelphia', true, 4),
            ('Pittsburgh', true, 4);