package com.example.demo.service;

import com.example.demo.exceprion.ErrorsFactory;
import com.example.demo.exceprion.Exception;
import com.example.demo.dto.RegionDto;
import com.example.demo.entity.Country;
import com.example.demo.entity.Region;
import com.example.demo.repository.RegionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class RegionService {

    @Autowired
    private RegionRepository regionRepository;

    @Transactional
    public List<RegionDto> getAllRegionsByCountryId(int id) {
        return regionRepository.findAllByCountryId(id).stream().map(map()).collect(Collectors.toList());
    }

    public Region findByNameAndCountry(String regionName, Country countryName, Exception error) {
        Region region = regionRepository.findByNameAndCountry(regionName,countryName);
        if(region == null){
            region = new Region();
            region.setName(regionName);
            region.setCountry(countryName);
            region.setModeration(false);
            regionRepository.save(region);
            if(error.getId() != ErrorsFactory.ENTIRY_NOT_FOUND_AND_WAS_CREATED) {
                error.setId(ErrorsFactory.ENTIRY_NOT_FOUND_AND_WAS_CREATED).setMessage("Region not found and was created ");
            }
        }
        return region;
    }

    private Function<Region, RegionDto> map() {
        return s -> (RegionDto) new RegionDto().setId(s.getId()).setName(s.getName());
    }


}
