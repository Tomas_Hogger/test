package com.example.demo.service;

import com.example.demo.exceprion.ErrorsFactory;
import com.example.demo.exceprion.Exception;
import com.example.demo.dto.CountryDto;
import com.example.demo.entity.Country;
import com.example.demo.repository.CountryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class CountryService {

    @Autowired
    private CountryRepository countryRepository;

    @Transactional
    public List<CountryDto> getAllCountries() {
        return countryRepository.findAll().stream().map(map()).collect(Collectors.toList());
    }


    public Country findByName(String countryName, Exception error) {
        Country country = countryRepository.findByName(countryName);
        if(country == null){
            country = new Country();
            country.setName(countryName);
            country.setModeration(false);
            countryRepository.save(country);
            error.setId(ErrorsFactory.ENTIRY_NOT_FOUND_AND_WAS_CREATED).setMessage("Country not found and was created ");
        }
        return country;
    }

    private Function<Country, CountryDto> map() {
        return s -> (CountryDto) new CountryDto().setId(s.getId()).setName(s.getName());
    }
}
