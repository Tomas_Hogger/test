package com.example.demo.service;

import com.example.demo.exceprion.ErrorsFactory;
import com.example.demo.controller.Helpers.ControllerHelper;
import com.example.demo.entity.City;
import com.example.demo.entity.User;
import com.example.demo.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.demo.exceprion.Exception;

import javax.transaction.Transactional;
import java.util.Date;

@Service
public class UserService {


    @Autowired
    private UserRepository userRepository;

    @Transactional
    public int getScore(String token) throws Exception {
        User user = userRepository.findByToken(token);
        if(user == null)
            throw new Exception().setId(ErrorsFactory.TOKEN_DOESNT_EXIST).setMessage("Token doesn't exist");
        return user.getScore();
    }

    @Transactional
    public void addBonus(String token, int bonus) throws Exception {
        Exception exception = new Exception();
        User user = userRepository.findByToken(token);
        if(user == null)
            throw exception.setId(ErrorsFactory.TOKEN_DOESNT_EXIST).setMessage("Token doesn't exist");
        if(user.getCity() == null)
            throw exception.setId(ErrorsFactory.TOKEN_HASNT_CITY).setMessage("User has no city and therefore cannot add bonus");

        long time = System.currentTimeMillis();
        if (user.getLast_update() == null || user.getLast_update().getTime() - System.currentTimeMillis() >= 122400000) {
            user.setScore(user.getScore() + bonus);
            user.setLast_update(new Date(time));
            userRepository.save(user);
        } else {
            throw exception.setId(ErrorsFactory.LITTLE_TIME_HAS_PASSED).setMessage("It takes more time to get a bonus.");
        }

    }

    public void createNewUser(String token) {
        User user = new User();
        user.setToken(token);
        userRepository.save(user);
    }

    @Transactional
    public void saveCityForUser(City city, String token) {
        User user = userRepository.findByToken(token);
        if(user == null) {
            user = new User();
            user.setToken(token);
        }
        user.setScore(ControllerHelper.FIRST_BONUS);
        user.setCity(city);
        userRepository.save(user);
    }




}
