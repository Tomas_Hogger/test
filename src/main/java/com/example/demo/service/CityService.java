package com.example.demo.service;

import com.example.demo.exceprion.ErrorsFactory;
import com.example.demo.dto.CityDto;
import com.example.demo.entity.City;
import com.example.demo.entity.Country;
import com.example.demo.entity.Region;
import com.example.demo.repository.CityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.example.demo.exceprion.Exception;


import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class CityService {

    @Autowired
    private CityRepository cityRepository;

    @Autowired
    private CountryService countryService;

    @Autowired
    private RegionService regionService;

    @Autowired
    private UserService userService;


    @Transactional
    public List<CityDto> getAllCitiesByRegionId(int id) {
        return cityRepository.findAllByRegionId(id).stream().map(map()).collect(Collectors.toList());
    }

    @Transactional
    public void saveCity(String countryName, String regionName, String cityName, String token) throws Exception {
        Exception exception = new Exception();
        Country country = countryService.findByName(countryName, exception);
        Region region = regionService.findByNameAndCountry(regionName,country,exception);
        City city = cityRepository.findByNameAndRegion(cityName,region);
        if(city == null){
            city = new City();
            city.setName(cityName);
            city.setRegion(region);
            city.setModeration(false);
            cityRepository.save(city);
            if(exception.getId() != ErrorsFactory.ENTIRY_NOT_FOUND_AND_WAS_CREATED) {
                exception.setId(ErrorsFactory.ENTIRY_NOT_FOUND_AND_WAS_CREATED).setMessage("City not found and was created ");
            }
            throw exception;
        } else {
            userService.saveCityForUser(city, token);
        }
    }

    private Function<City, CityDto> map() {
        return s -> (CityDto) new CityDto().setId(s.getId()).setName(s.getName());
    }



}
