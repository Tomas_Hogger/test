package com.example.demo.service;

import com.example.demo.repository.UserRepository;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class StaticService {

    private static final int VERSION = 1;


    @Autowired
    private UserRepository userRepository;


    @Transactional
    public int getLastUpdate(String token) {
        return VERSION;
    }

    @Transactional
    public JSONObject getJSONblock(String token) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("Name", "Test");
        jsonObject.put("Opening date", "10.05.2019");
        jsonObject.put("Version", VERSION);
        return jsonObject;
    }


}
