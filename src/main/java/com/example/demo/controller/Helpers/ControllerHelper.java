package com.example.demo.controller.Helpers;

import com.example.demo.dto.TokenDto;
import com.example.demo.exceprion.ErrorForUser;
import com.example.demo.exceprion.Exception;
import com.example.demo.service.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import javax.servlet.http.HttpSession;
import java.text.SimpleDateFormat;
import java.util.Random;
import java.util.UUID;

public class ControllerHelper {

    public static final SimpleDateFormat SIMPLE_DATE_FORMAT = new SimpleDateFormat("dd.MM.yyyy - HH:mm");
    public static final int MAX_BONUS = 10;
    public static final int MIN_BONUS = 10;
    public static final int FIRST_BONUS = 10;
    private static Random random = new Random();
    private static boolean check = true;

    static public String getToken(){
        return (UUID.randomUUID().toString() + UUID.randomUUID().toString() + UUID.randomUUID().toString().substring(10)).replaceAll("-", "");
    }

    public static int getBonus() {
        return MIN_BONUS + MIN_BONUS * random.nextInt(MAX_BONUS);
    }

    public static ResponseEntity token(HttpSession session, UserService authService){
        check = true;
        String token = token(session);
        TokenDto tokenDto = new TokenDto().setToken(token);
        if (!check) {
            authService.createNewUser(token);
            return new ResponseEntity(tokenDto, HttpStatus.CREATED);
        }
        return new ResponseEntity(tokenDto, HttpStatus.OK);
    }

    public static String token(HttpSession session) {
        String token = (String) session.getAttribute("token");
        if (token == null){
            check = false;
            token = ControllerHelper.getToken();
            session.setAttribute("token", token);
        }
        return token;
    }

    public static ErrorForUser toUserError(Exception error){
        return new ErrorForUser().setId(error.getId()).setMessage(error.getMessage());
    }
}
