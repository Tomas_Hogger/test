package com.example.demo.controller;


import com.example.demo.controller.Helpers.ControllerHelper;
import com.example.demo.dto.CityDto;
import com.example.demo.service.CityService;
import com.example.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import com.example.demo.exceprion.Exception;


import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
public class CityController {

    @Autowired
    private CityService cityService;

    @Autowired
    private UserService userService;


    @GetMapping("/cities")
    @ResponseBody
    public List<CityDto> cities(@RequestParam(value = "id") int id){
        return cityService.getAllCitiesByRegionId(id);
    }




    @GetMapping("/city")
    @ResponseBody
    public String city(HttpSession session, @RequestParam(value = "country") String country, @RequestParam(value = "region") String region, @RequestParam(value = "city") String city) throws Exception {
        String token = ControllerHelper.token(session);
        cityService.saveCity(country,region,city,token);
        return "CityForController " + city + " save for user with token " + token;
    }


    @ExceptionHandler(Exception.class)
    public ResponseEntity cityExceptionHandler(Exception e){
        return new ResponseEntity(ControllerHelper.toUserError(e), HttpStatus.CREATED);
    }

}
