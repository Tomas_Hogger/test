package com.example.demo.controller;

import com.example.demo.dto.RegionDto;
import com.example.demo.service.RegionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
public class RegionController {

    @Autowired
    private RegionService regionService;


    @GetMapping("/regions")
    @ResponseBody
    public List<RegionDto> regions(@RequestParam(value = "id") int id){
        return regionService.getAllRegionsByCountryId(id);
    }

}
