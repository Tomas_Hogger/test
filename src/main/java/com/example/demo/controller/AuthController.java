package com.example.demo.controller;


import com.example.demo.controller.Helpers.ControllerHelper;
import com.example.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;

@Controller
public class AuthController {

    @Autowired
    private UserService authService;

    @GetMapping("/token")
    @ResponseBody
    public ResponseEntity token(HttpSession session){
        return ControllerHelper.token(session, authService);
    }





}
