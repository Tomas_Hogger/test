package com.example.demo.controller;

import com.example.demo.service.StaticService;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class StaticController {


    @Autowired
    private StaticService jsonService;

    @GetMapping("/version")
    @ResponseBody
    public int last_update(@RequestParam(value = "token") String token){
        return jsonService.getLastUpdate(token);
    }


    @GetMapping("static_block")
    @ResponseBody
    public JSONObject static_block(@RequestParam(value = "token") String token){
        return jsonService.getJSONblock(token);
    }


}
