package com.example.demo.controller;

import com.example.demo.controller.Helpers.ControllerHelper;
import com.example.demo.dto.ScoreDto;
import com.example.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import com.example.demo.exceprion.Exception;


@Controller
public class BonusController {

    @Autowired
    private UserService authService;


    @GetMapping("/score")
    @ResponseBody
    public ResponseEntity token(@RequestParam(value = "token") String token) throws Exception {
        int score = authService.getScore(token);
        return new ResponseEntity(new ScoreDto().setScore(score), HttpStatus.OK);
    }

    @GetMapping("/bonus")
    @ResponseBody
    public int bonus(@RequestParam(value = "token") String token) throws Exception {
        int bonus = ControllerHelper.getBonus();
        authService.addBonus(token,bonus);
        return bonus;
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity bonusExceptionHandler(Exception e){
        return new ResponseEntity(ControllerHelper.toUserError(e), HttpStatus.OK);
    }

}
