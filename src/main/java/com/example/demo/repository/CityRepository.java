package com.example.demo.repository;

import com.example.demo.entity.City;
import com.example.demo.entity.Region;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface CityRepository extends JpaRepository<City, Long> {

    City findByNameAndRegion(String city, Region region);

    @Query(value = "SELECT * FROM City c where c.is_Moderation = true AND c.region_id = ?1", nativeQuery = true)
    List<City> findAllByRegionId(int id);

    List<City> findAllByRegionName(String name);
}
