package com.example.demo.repository;

import com.example.demo.entity.Country;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface CountryRepository extends JpaRepository<Country, Long> {

    Country findByName(String country);

    @Query(value = "SELECT * FROM Country c where c.is_Moderation = true", nativeQuery = true)
    List<Country> findAll();

}
