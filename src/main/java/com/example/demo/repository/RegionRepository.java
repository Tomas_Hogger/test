package com.example.demo.repository;

import com.example.demo.entity.Country;
import com.example.demo.entity.Region;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface RegionRepository extends JpaRepository<Region, Long> {



    Region findByNameAndCountry(String region, Country country);

    @Query(value = "SELECT * FROM region r WHERE r.is_moderation = true AND r.country_id = ?1",nativeQuery = true)
    List<Region> findAllByCountryId(int id);

    List<Region> findAllByCountryName(String name);
}
