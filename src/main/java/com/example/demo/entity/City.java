package com.example.demo.entity;

import javax.persistence.Entity;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table
public class City implements com.example.demo.entity.Entity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(nullable = false)
    private String name;

    @Column
    private Boolean isModeration;

    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Region region;

    @OneToMany(mappedBy = "city", fetch = FetchType.LAZY)
    private List<User> user = new ArrayList<>();

    public void setId(int id){
        this.id = id;
    }

    public int getId(){
        return id;
    }

    public void setName(String name){
        this.name = name;
    }

    public String getName(){
        return name;
    }

    public Boolean getModeration() {
        return isModeration;
    }

    public void setModeration(Boolean moderation) {
        isModeration = moderation;
    }

    public Region getRegion() {
        return region;
    }

    public void setRegion(Region region) {
        this.region = region;
    }

    public List<User> getUser() {
        return user;
    }

    public void setUser(List<User> user) {
        this.user = user;
    }
}
