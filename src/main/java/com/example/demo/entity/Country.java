package com.example.demo.entity;

import javax.persistence.Entity;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "country")
public class Country implements com.example.demo.entity.Entity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(unique = true, nullable = false)
    private String name;

    @OneToMany(mappedBy = "country", fetch = FetchType.LAZY)
    private List<Region> regions = new ArrayList<>();

    @Column
    private Boolean isModeration;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Region> getRegions(){
        return regions;
    }

    public void setRegions(List<Region> regions) {
        this.regions = regions;
    }

    public Boolean getModeration() {
        return isModeration;
    }

    public void setModeration(Boolean moderation) {
        isModeration = moderation;
    }
}
