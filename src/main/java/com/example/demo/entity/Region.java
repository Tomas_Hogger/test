package com.example.demo.entity;

import javax.persistence.Entity;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table
public class Region implements com.example.demo.entity.Entity {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(nullable = false)
    private String name;

    @OneToMany(mappedBy = "region", fetch = FetchType.LAZY)
    private List<City> cities = new ArrayList<>();

    @ManyToOne(optional = false,fetch = FetchType.LAZY)
    private Country country;

    @Column
    private Boolean isModeration;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public List<City> getCity(){
        return cities;
    }

    public void setCity(List<City> cities){
        this.cities = cities;
    }

    public Boolean getModeration() {
        return isModeration;
    }

    public void setModeration(Boolean moderation) {
        isModeration = moderation;
    }
}
