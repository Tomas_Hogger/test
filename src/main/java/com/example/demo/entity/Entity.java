package com.example.demo.entity;

public interface Entity {
    int getId();
    String getName();
}
