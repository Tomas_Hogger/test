package com.example.demo.exceprion;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class ErrorForUser {
    int id = 0;
    String message = null;
}
